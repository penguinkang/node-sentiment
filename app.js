/****
* TweetStream Vis
* Byungkyu (Jay) Kang
* Feb. 26. 2015
****/

// required modules
var express = require('express');
var sentiment = require('sentiment');
var Twitter = require('twitter');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();
var stream;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/index', routes);
app.use('/users', users);

// glbal vars
var tweetCount = 0;
var tweetTotalSentiment = 0;
var monitoringPhrase;

/** functions **/
// reset stream
function resetMonitoring() {
    if (stream) {
        var tempStream = stream;
        stream = null;  // signal to event handlers to ignore end/destroy
        tempStream.destroySilent();
    }
    monitoringPhrase = "";
}

// open a new stream
// function beginMonitoring(phrase) {
function beginMonitoring(phrase) {
    console.log("started!");

    // cleanup if we're re-setting the monitoring
    if (monitoringPhrase) {
        resetMonitoring();
    }

    // reset vars
    monitoringPhrase = phrase;
    tweetCount = 0;
    tweetTotalSentiment = 0;

    // open a new stream
    stream = client.stream('statuses/filter', {
        track: monitoringPhrase
    }, function(inStream) {
        stream = inStream;
        console.log("Monitoring Twitter for " + monitoringPhrase);

        // print message + keyword
        // res.send("Monitoring Twitter for \'" + monitoringPhrase);

        // process tweet
        stream.on('data', function(tweet) {
            // console.log(tweet.text);

            // English only
            if (tweet.lang === 'en') {
                console.log(tweet.text);
                sentiment(tweet.text, function (err, result) {
                    tweetCount++;
                    tweetTotalSentiment += result.score;
                });
            }
            // // Update the console every 50 analyzed tweets
            // if (testTweetCount % 50 === 0) {
            //     console.log("Tweet #" + testTweetCount + ": " + tweet.text);
            // }
        });

        // exception
        stream.on('error', function(error) {
            throw error;
        });
    });

    return stream;
};

function sentimentImage() {
    var avg = tweetTotalSentiment / tweetCount;
    if (avg > 0.5) { // pos
        return "http://2.bp.blogspot.com/-OsnLCK0vg6Y/UZD8pZha0NI/AAAAAAAADnY/sViYKsYof-w/s1600/big-smile-emoticon-for-facebook.png";
    }
    if (avg < -0.5) { // neg
        return "http://2.bp.blogspot.com/-rnfZUujszZI/UZEFYJ269-I/AAAAAAAADnw/BbB-v_QWo1w/s1600/facebook-frown-emoticon.png";
    }
    // neutral
    return "http://3.bp.blogspot.com/-wn2wPLAukW8/U1vy7Ol5aEI/AAAAAAAAGq0/f7C6-otIDY0/s1600/squinting-emoticon.png";
}

 

// twitter connection 
var client = new Twitter({
  consumer_key: 'qUl3k3LLjp6kF8DpTTqsUlSAi',
  consumer_secret: '9LkwGFQfkPzadDcoBLHOx7UONtOroSv1OwKNC31hjXb4YbnXeZ',
  access_token_key: '3045060138-z1zMc8HCi4xTLqdmlbmOSWPGCQnQNbNdXVZjFXd',
  access_token_secret: '8ezKLTDoYr2oE1fhb2Cv65GFGMhbaRTmVWv7Ke4Xrjdfz'
});


app.get('/sentiment', function (req, res) {
    res.json({monitoring: (monitoringPhrase != null), 
        monitoringPhrase: monitoringPhrase, 
        tweetCount: tweetCount, 
        tweetTotalSentiment: tweetTotalSentiment,
        sentimentImageURL: sentimentImage()});
});

app.post('/sentiment', function (req, res) {
    try {
        if (req.body.phrase) {
            beginMonitoring(req.body.phrase);
            res.send(200);          
        } else {
            res.status(400).send('Invalid request: send {"phrase": "bieber"}');     
        }
    } catch (exception) {
        res.status(400).send('Invalid request: send {"phrase": "bieber"}');
    }
});

app.get('/watchTwitter', function (req, res) {
    var stream;
    var testTweetCount = 0;
    var phrase = 'isis';

        // stream = tweeter.stream('statuses/filter', {
        //     'track': phrase
        // }, function (stream) {
    stream = client.stream('statuses/filter', {
        track: phrase
    }, function(stream) {
        // print message + keyword
        res.send("Monitoring Twitter for \'" + phrase
            + "\'... Logging Twitter traffic.");

        // process tweet
        stream.on('data', function(tweet) {
            // console.log(tweet.text);
            testTweetCount++;
            // Update the console every 50 analyzed tweets
            if (testTweetCount % 50 === 0) {
                console.log("Tweet #" + testTweetCount + ": " + tweet.text);
            }
        });

        // exception
        stream.on('error', function(error) {
            throw error;
        });
    });
});

// interface
app.get('/', 
    function (req, res) {
        console.log("test1");
        var welcomeResponse = "<HEAD>" +
            "<title>Twitter Sentiment Analysis</title>\n" +
            "</HEAD>\n" +
            "<BODY>\n" +
            "<P>\n" +
            "Welcome to the Twitter Sentiment Analysis app.<br>\n" +
            "What would you like to monitor?\n" +
            "</P>\n" +
            "<FORM action=\"/monitor\" method=\"get\">\n" +
            "<P>\n" +
            "<INPUT type=\"text\" name=\"phrase\"><br><br>\n" +
            "<INPUT type=\"submit\" value=\"Go\">\n" +
            "</P>\n" + "</FORM>\n" + "</BODY>";
        if (!monitoringPhrase) {
            res.send(welcomeResponse);
        } else {
            console.log("test2");
            var monitoringResponse = "<HEAD>" +
                "<META http-equiv=\"refresh\" content=\"5; URL=http://" +
                req.headers.host +
                "/\">\n" +
                "<title>Twitter Sentiment Analysis</title>\n" +
                "</HEAD>\n" +
                "<BODY>\n" +
                "<P>\n" +
                "The Twittersphere is feeling<br>\n" +
                "<IMG align=\"middle\" src=\"" + sentimentImage() + "\"/><br>\n" +
                "about " + monitoringPhrase + ".<br><br>" +
                "Analyzed " + tweetCount + " tweets...<br>" +
                "</P>\n" +
                "<A href=\"/reset\">Monitor another phrase</A>\n" +
                "</BODY>";
            res.send(monitoringResponse);
        }
});



// test sentiment module
app.get('/testSentiment',
    function (req, res) {
        var response = "<HEAD>" +
            "<title>Twitter Sentiment Analysis</title>\n" +
            "</HEAD>\n" +
            "<BODY>\n" +
            "<P>\n" +
            "Welcome to the Twitter Sentiment Analysis app. " +
            "What phrase would you like to analzye?\n" +
            "</P>\n" +
            "<FORM action=\"/testSentiment\" method=\"get\">\n" +
            "<P>\n" +
            "Enter a phrase to evaluate: <INPUT type=\"text\" name=\"phrase\"><BR>\n" +
            "<INPUT type=\"submit\" value=\"Send\">\n" +
            "</P>\n" +
            "</FORM>\n" +
            "</BODY>";
        var phrase = req.query.phrase;
        if (!phrase) {
            res.send(response);
        } else {
            sentiment(phrase, function (err, result) {
            response = 'sentiment(' + phrase + ') === ' + result.score;
            res.send(response);
            });
        }
    });

app.get('/monitor', function (req, res) {
    beginMonitoring(req.query.phrase);
    res.redirect(302, '/');
});

app.get('/reset', function (req, res) {
    resetMonitoring();
    res.redirect(302, '/');
});

app.get('/hello', function (req, res) {
    res.send("Hello world.");
});



// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});




// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});




// app.listen(3000);
module.exports = app;
